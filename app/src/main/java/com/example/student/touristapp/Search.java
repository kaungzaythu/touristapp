package com.example.student.touristapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Search extends ActionBarActivity implements View.OnClickListener {
    Button btnsearch;
    EditText txtSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        btnsearch = (Button) findViewById(R.id.btnSearch);
        txtSearch = (EditText) findViewById(R.id.txtSearch);
        btnsearch.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
           Intent i = new Intent(this, Snap.class);
           startActivity(i);
           return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        String search = txtSearch.getText().toString();

        int a = search.indexOf(' ');
        System.out.println(a);
        String b = search.substring(0,a);
        int c = a + 1;
        String d = search.substring(c);
        String e = b + "%20"+ d;

        System.out.println(e);
        Gallery g = Gallery.GetSearch(e);
        if (g ==null){
            Toast.makeText(this,"Search Result Not Found",Toast.LENGTH_LONG).show();
            txtSearch.setText("");
            //txtSearch.focus
        }
        else
        {
        Intent intent = new Intent(this, GalleryDetail.class);
        intent.putExtra("details", g);
        startActivity(intent);
    }}
}
