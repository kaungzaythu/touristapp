package com.example.student.touristapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.List;


public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{

    final static int ACTION_EDIT = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //network on makin thread exception
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        final ListView list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(this);
        new AsyncTask<Void, Void, List<Gallery>>(){

            @Override
            protected List<Gallery> doInBackground(Void... params) {
                return Gallery.jread();
            }


           protected void onPostExecute(List<Gallery> result){

                   MyAdapter adapter = new MyAdapter(MainActivity.this, R.layout.row2, result);
                   list.setAdapter(adapter);

            }
        }.execute();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, Snap.class);
            startActivity(i);
            return true;
        }
        else if(id == R.id.edit)
        {
           Intent i = new Intent(this, Edit.class);
            startActivity(i);
            return true;
        }
        else if(id == R.id.search)
        {
            Intent i = new Intent(this, Search.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> av, View view, int position, long id) {


        Gallery g = (Gallery) av.getAdapter().getItem(position);
        Intent intent = new Intent(this, GalleryDetail.class);
        intent.putExtra("details", g);
        startActivity(intent);

    }
}
