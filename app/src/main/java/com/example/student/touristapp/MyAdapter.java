package com.example.student.touristapp;

/**
 * Created by Kaung Zay Thu on 26/1/15.
 */
 import android.app.Activity;
 import android.content.Context;
 import android.graphics.Bitmap;
 import android.graphics.BitmapFactory;
 import android.os.AsyncTask;
 import android.util.Log;
 import android.view.LayoutInflater;
 import android.view.View;
 import android.view.ViewGroup;
 import android.widget.ArrayAdapter;
 import android.widget.ImageView;
 import android.widget.TextView;
 import java.io.InputStream;
 import java.net.URL;
 import java.util.List;


public class MyAdapter extends ArrayAdapter<Gallery> {

    private List<Gallery> gallery;

    public MyAdapter(Context context, int resource, List<Gallery> gallery) {
        super(context, resource, gallery);
        this.gallery = gallery;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row2, null);//
        Gallery p = gallery.get(position);

        if (p != null) {
            TextView id = (TextView) v.findViewById(R.id.txt1);
            id.setText(p.get("id"));
            TextView user_name = (TextView) v.findViewById(R.id.txt2);
            user_name.setText(p.get("user_name"));
            final TextView images = (TextView) v.findViewById(R.id.text3);
            images.setText(p.get("image"));
            TextView description = (TextView) v.findViewById(R.id.text4);
            description.setText(p.get("description"));
            //TextView location = (TextView) v.findViewById(R.id.text5);
            //location.setText(p.get("location"));
            TextView txtloc = (TextView) v.findViewById(R.id.txtloc);
            txtloc.setText(p.get("location"));


            final ImageView image = (ImageView) v.findViewById(R.id.imageView);

            new AsyncTask<String, Void, Bitmap>() {
                Bitmap b;
                @Override
                protected Bitmap doInBackground(String... images) {
                    String url=String.format("http://192.168.38.127/tourist/myimage.aspx?image=%s&size=100",images[0]);
                    try {
                        InputStream in = new URL(url).openStream();
                        b = BitmapFactory.decodeStream(in);
                    } catch (Exception ex) {
                        Log.e("Bitmap Error", ex.toString());
                    }
                    return b;
                }
                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    image.setImageBitmap(bitmap);
                }
            }.execute(p.get("image"));

        }
        return v;
    }
}