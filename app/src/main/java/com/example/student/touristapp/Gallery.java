package com.example.student.touristapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kaung Zay Thu on 27/1/15.
 */
public class Gallery extends HashMap<String, String> {

    final static String baseurl = "http://192.168.38.127/tourist";
    public Gallery(String id, String user_name, String image, String description, String location ) {
        put("id", id);
        put("user_name", user_name);
        put("image", image);
        put("description", description );
        put("location", location);

    }
    public static List<String> list() {
        List<String> list = new ArrayList<String>();
        JSONArray a = JSONParser.getJSONArrayFromUrl(String.format("%s/Service.svc/List", baseurl));
        try {
            for (int i =0; i<a.length(); i++) {
                String b = a.getString(i);
                list.add(b);
            }
        } catch (Exception e) {
            Log.e("list", "JSONArray error");
        }
        return(list);
    }
    public static Gallery getGallery(String id) {
        Gallery p = null;
        try {
            JSONObject a = JSONParser.getJSONFromUrl(String.format("%s/Service.svc/GetGallery/%s", baseurl, id));
            p = new Gallery(a.getString("Id"), a.getString("User_name"), a.getString("Image"), a.getString("Description"), a.getString("Location"));
        } catch (Exception e) {
            Log.e("getGallery", "JSON error");
        }
        return p;
    }
    public static Gallery GetSearch(String id) {
        Gallery p = null;
        try {
            JSONObject a = JSONParser.getJSONFromUrl(String.format("%s/Service.svc/GetSearch/%s", baseurl, id));
            p = new Gallery(a.getString("Id"), a.getString("User_name"), a.getString("Image"), a.getString("Description"), a.getString("Location"));
        } catch (Exception e) {
            Log.e("getGallery", "JSON error");
        }
        return p;
    }

    public static List<Gallery> jread() {
        String url = "http://192.168.38.127/tourist/Service.svc/List"; //http://192.168.38.127/tourist/Service.svc/List
        String url2 = "http://192.168.38.127/tourist/Service.svc/GetGallery";
        List<Gallery> list = new ArrayList<Gallery>();
        JSONArray a = JSONParser.getJSONArrayFromUrl(url);
        try {
            for (int i =0; i<a.length(); i++) {
                String id = a.getString(i);
                Log.i("Gallery", id);
                JSONObject obj = JSONParser.getJSONFromUrl(String.format("%s/%s", url2, id));
                list.add(new Gallery(obj.getString("Id"), obj.getString("User_name"),
                        obj.getString("Image"), obj.getString("Description"), obj.getString("Location")));
            }
        } catch (Exception e) {
            Log.e("Gallery", "JSONArray error !!!");
        }
        return(list);
    }


    public static void updateGallery(String id, String user_name, String image, String description, String location) {
        try {
            JSONObject gallery = new JSONObject();
            gallery.put("Id", id);
            gallery.put("User_name", user_name);
            gallery.put("Image", image);
            gallery.put("Description", description);
            gallery.put("Location", location);
            String json = gallery.toString();
            String result = JSONParser.postStream(
                    String.format("%s/Service.svc/UpdateGallery", baseurl),
                    json);
        } catch (Exception e) {
            Log.e("updateGallery", "JSON error");
        }
    }


    public static void insertGallery(String id, String user_name, String image, String description, String location) {
        try {
            JSONObject gallery = new JSONObject();
            gallery.put("Id", id);
            gallery.put("User_name", user_name);
            gallery.put("Image", image);
            gallery.put("Description", description);
            gallery.put("Location", location);
            String json = gallery.toString();
            String result = JSONParser.postStream(
                    String.format("%s/Service.svc/InsertGallery", baseurl),json);
        } catch (Exception e) {
            Log.e("insertGallery", "JSON error");
        }
    }

    public static  void DeleteGallery(String id)
    {
        String result = JSONParser.getStream(
                String.format("%s/Service.svc/DeleteGallery/%s", baseurl,id));
    }

    public static String getGalleryLatestID()
    {
       String b="";


        JSONArray a = JSONParser.getJSONArrayFromUrl(String.format("%s/Service.svc/GetGalleryLatestID", baseurl));
        try {
            for (int i =0; i<a.length(); i++) {
                b = a.getString(i);

            }

        } catch (Exception e) {
            Log.e("list", "JSONArray error");
        }
        return(b);
    }
   /* public static String Search(String s)
    {
        JSONArray a = JSONParser.getJSONArrayFromUrl(String.format("%s/Service.svc/Search", baseurl));
    }*/
}
