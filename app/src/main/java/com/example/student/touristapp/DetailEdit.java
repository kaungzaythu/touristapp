package com.example.student.touristapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;


public class DetailEdit extends ActionBarActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_edit);
        Button b = (Button) findViewById(R.id.btnupdate);
        b.setOnClickListener(this);
        Button b1 = (Button) findViewById(R.id.btndelete);
        b1.setOnClickListener(this);

        HashMap<String,String> obj = (HashMap) getIntent().getSerializableExtra("details");
        TextView t1 = (TextView) findViewById(R.id.etxt1);
        TextView t2 = (TextView) findViewById(R.id.etxt2);
        TextView t3 = (TextView) findViewById(R.id.etxt3);
        TextView t4 = (TextView) findViewById(R.id.etxt4);
        TextView t5 = (TextView) findViewById(R.id.etxt5);

        t1.setText(obj.get("id"));
        t2.setText(obj.get("user_name"));
        t4.setText(obj.get("description"));
        t3.setText(obj.get("location"));
        t5.setText(obj.get("image"));
        t1.setEnabled(false);
        t3.setEnabled(false);



        final ImageView image = (ImageView) findViewById(R.id.imageView);
        new AsyncTask<String, Void, Bitmap>() {
            Bitmap b;
            @Override
            protected Bitmap doInBackground(String... images) {
                String url=String.format("http://192.168.38.127/tourist/myimage.aspx?image=%s&size=100",images[0]);
                try {
                    InputStream in = new URL(url).openStream();
                    b = BitmapFactory.decodeStream(in);
                } catch (Exception ex) {
                    Log.e("Bitmap Error", ex.toString());
                }
                return b;
            }
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                image.setImageBitmap(bitmap);
            }
        }.execute(obj.get("image"));


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_edit, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnupdate) {
            EditText e = (EditText) findViewById(R.id.etxt1);
            String s1 = e.getText().toString();

            e = (EditText) findViewById(R.id.etxt2);
            String s2 = e.getText().toString();

            e = (EditText) findViewById(R.id.etxt5);
            String s3 = e.getText().toString();

            e = (EditText) findViewById(R.id.etxt4);
            String s4 = e.getText().toString();

            e = (EditText) findViewById(R.id.etxt3);
            String s5 = e.getText().toString();

            new AsyncTask<String, Void, Void>() {
                @Override
                protected Void doInBackground(String... params) {
                    Gallery.updateGallery(params[0], params[1], params[2], params[3], params[4]);
                    return null;
                }
                @Override
                protected void onPostExecute(Void v) {
                    setResult(RESULT_OK);
                    finish();
                }
            }.execute(s1, s2, s3, s4,s5);

            Toast.makeText(this, "Successfully update :)", Toast.LENGTH_LONG).show();


        }else if (v.getId() == R.id.btndelete)
        {
            EditText e = (EditText) findViewById(R.id.etxt1);
            String s1 = e.getText().toString();

            new AsyncTask<String, Void, Void>() {
                @Override
                protected Void doInBackground(String... params) {
                    Gallery.DeleteGallery(params[0]);
                    return null;
                }
                @Override
                protected void onPostExecute(Void v) {
                    setResult(RESULT_OK);
                    finish();
                }
            }.execute(s1);
            Toast.makeText(this, "Delete success :)", Toast.LENGTH_LONG).show();
        }


        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();


    }
}
