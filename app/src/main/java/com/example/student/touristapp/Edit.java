package com.example.student.touristapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;


public class Edit extends ActionBarActivity implements AdapterView.OnItemClickListener {

    final static int ACTION_EDIT = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        //network on makin thread exception
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        final ListView list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(this);
        new AsyncTask<Void, Void, List<Gallery>>(){

            @Override
            protected List<Gallery> doInBackground(Void... params) {
                return Gallery.jread();
            }


            protected void onPostExecute(List<Gallery> result){

                MyAdapter adapter = new MyAdapter(Edit.this, R.layout.row2, result);
                list.setAdapter(adapter);

            }
        }.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> av, View view, int position, long id) {
       /* String item = (String) av.getAdapter().getItem(position);
        Intent intent = new Intent(this, DetailEdit.class);
        intent.putExtra("ID", item);
        startActivityForResult(intent, ACTION_EDIT);*/
        Gallery g = (Gallery) av.getAdapter().getItem(position);
        Intent intent = new Intent(this, DetailEdit.class);
        intent.putExtra("details", g);
        startActivity(intent);

    }
}
