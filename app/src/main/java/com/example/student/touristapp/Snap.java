package com.example.student.touristapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Locale;


public class Snap extends ActionBarActivity implements View.OnClickListener{
    boolean check = false;
    String fileName="";
    File file = null;
   // static final String fileName="";
    Uri uri = null;
    boolean supplyFile = true;
    final static int CAPTURE_IMAGE_REQUEST_CODE = 101;
    MyLocation ml;

    TextView txtloc;
    EditText e1,e2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap);
        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.button2);
        b.setOnClickListener(this);
        Button btnloc = (Button) findViewById(R.id.btnLocation);
        btnloc.setOnClickListener(this);
        txtloc = (TextView) findViewById(R.id.txtLocation);


        e1 = (EditText) findViewById(R.id.txt1);
        String s2 = e1.getText().toString();
        e2 = (EditText) findViewById(R.id.txt2);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button) {
              capturePhoto();
           // }
           // else
           // {
           //    Toast.makeText(this,"Plase Enter Title and Description :)",Toast.LENGTH_LONG).show();
           // }

        } else if (v.getId() == R.id.button2) {
           /* String y = e1.getText().toString();
            System.out.println(y);
            if(y ==""){
                Toast.makeText(this,"Please Enter Title and Description :)",Toast.LENGTH_LONG).show();
            }else
            {*/
                uploadPhoto();
           // }

            //if()
           // if(e.getText().toString() == null) {


        }
        else
        {
            currentlocation();
        }
    }

    private void capturePhoto() {
        String mili = String.valueOf(System.currentTimeMillis());

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (supplyFile) {
            //file = new File("/sdcard/", "IMG_" +String.valueOf(System.currentTimeMillis()) + ".jpg");
            file = new File("/sdcard/", "IMG_" + mili + ".jpg");
            fileName = "IMG_" + mili + ".jpg";
            uri = Uri.fromFile(file);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        }
        startActivityForResult(intent, CAPTURE_IMAGE_REQUEST_CODE);
    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bitmap bmp = null;
                if (supplyFile) {
                    try {
                        FileInputStream fis = new FileInputStream(file);
                        bmp = BitmapFactory.decodeStream(fis);
                    } catch (Exception e) {
                        Log.i("pics", e.toString());
                    }
                } else {
                    uri = data.getData();
                    bmp = (Bitmap) data.getExtras().get("data");
                }
                ImageView image = (ImageView) findViewById(R.id.imageView);
                image.setImageBitmap(bmp);
            }
        }
    }

    private void uploadPhoto() {



        new AsyncTask<Uri, Void, Void>(){
            @Override
            protected Void doInBackground(Uri... arg) {
                String f = getRealPathFromURI(arg[0]);
                Transfer.uploadFile(f);
                return null;
            }
            protected void onPostExecute(Void v) {
                new AsyncTask<Uri, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Uri... arg) {
                        File f = new File(getRealPathFromURI(arg[0]));
                        String n = f.getName();
                        //fileName = f.getName();
                        try {
                            InputStream in = new URL(String.format("%s/images/%s", Transfer.base, n)).openStream();
                            Bitmap b = BitmapFactory.decodeStream(in);
                            return b;
                        } catch (Exception e) {
                            return null;
                        }
                    }

                    protected void onPostExecute(Bitmap b) {
                        ImageView v = (ImageView) findViewById(R.id.imageView2);
                        v.setImageBitmap(b);
                    }
                }.execute(uri);
            }
        }.execute(uri);


       // e1 = (EditText) findViewById(R.id.txt1);
        String s2 = e1.getText().toString();
      //  e2 = (EditText) findViewById(R.id.txt2);
        String s4 = e2.getText().toString();

        String a = Gallery.getGalleryLatestID();
        int b = Integer.parseInt(a);
        int c = b + 1;
        String s1 = String.format("%s", c);

        //String s1 = "14";

        String s3 = String.format("%s",fileName);

        String s5 = txtloc.getText().toString();

        new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... params) {
                Gallery.insertGallery(params[0], params[1], params[2], params[3], params[4]);
                return null;
            }
            @Override
            protected void onPostExecute(Void v) {
                setResult(RESULT_OK);
                finish();
            }
        }.execute(s1, s2, s3, s4, s5);

        Toast.makeText(this,"Uploaded :)",Toast.LENGTH_LONG).show();

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        //return true;
    }

    private String getRealPathFromURI(Uri uri)
    {
        String filePath;
        if (uri != null && "content".equals(uri.getScheme())) {
            Cursor cursor = getContentResolver().
                    query(uri, new String[] { android.provider.MediaStore.Images.ImageColumns.DATA }, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = uri.getPath();
        }
        return(filePath);
    }
    private void currentlocation() {
        ml = new MyLocation(Snap.this);
        if (ml.canGetLocation()) {
            double latitude = ml.getLatitude();
            double longitude = ml.getLongitude();
            String location;
            Geocoder geocoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(
                        latitude, longitude, 1);
                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    location = address.getAddressLine(0);
                    //System.out.print(location);
                    txtloc.setText(String.format(location));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ml.showSettingsAlert();
        }
    }
}
